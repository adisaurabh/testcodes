import numpy as np
from scipy import signal
from matplotlib import mlab

def fn_spec(ts,fs):
	'get psd using 50% ovlap and boxcar window'
	nfft = 3*fs
	novlp = 0.5*fs
	win = signal.get_window('hann',nfft)

	# get power
	spec, f = mlab.csd(ts,ts,NFFT=nfft,Fs=fs,window=win,noverlap=novlp)
	SP = np.column_stack((f,abs(spec)))

	return SP
