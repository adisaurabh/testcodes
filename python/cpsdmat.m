% cpsd implementation in matlab
function [ff,P] = cpsdmat(x,y,fs,nwin,nfft,ovlap)

L	= numel(x);
N 	= nfft*fs;
W   = nwin*fs;
ff	= fs/2*linspace(0,1,N/2+1);
df  = ff(2)-ff(1);
P 	= zeros(numel(ff),1);
stpsize	= W - W*ovlap;
winidx	= 1:stpsize:(L-W+1);
zeromat = zeros(N-W,1);
for ii= 1:1:numel(winidx)
	id = winidx(ii);
	xx = x(id: id+W-1);
	yy = y(id: id+W-1);
	xx = detrendlin(xx);
	yy = detrendlin(yy);
   	xx = applhann(xx);
	yy = applhann(yy);
	xx = [xx; zeromat];
 	yy = [yy; zeromat];
	XX = fft(xx)*(sqrt(8/3));   % coefficient correction: hann
	YY = fft(yy)*(sqrt(8/3));   % coefficient correction: hann
	Pxy = conj(XX(1:N/2+1)).*YY(1:N/2+1);
	Pxy = Pxy * 2 /(N*W*df);       % incorporate -ve freqs (x2), window length
	P = P + Pxy;
end
P	= P/numel(winidx);

function y = detrendlin(x)
	y = x - mean(x);
end

function y = applhann(x)
	win = hann(numel(x));
	y = x.*win;
end

end
