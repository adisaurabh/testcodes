"""
@file	fn_cpsd
@version	1.0
@author	Aditya Saurabh
@date 	February 05, 2015
@brief	Cross-spectral density between two 1-D time series
@detail	--todo--
"""
import numpy as np

def detrendlin(ts):
	'right now its just mean subtraction'
	tsd	= ts - np.mean(ts)
	return tsd

def applyhanning(ts):
	winsize	= len(ts)
	win	= np.hanning(winsize)
	tsw	= ts*win
	return tsw

def getfft(y):
	Y	= (np.fft.fft(y))
	Y	= Y*(8./3)**0.5
	return Y

def writecpsd(fname,ff,data):
	with open(fname,'w') as fid:
		np.savetxt(fid,np.hstack((ff,data.real,data.imag)),
				delimiter='\t',fmt='%g %g %g')

def main(y, x, fs=8192, nfft=1, ovlap=0.5):
	'''
	arguments nwin and novlap are in [0,1], expressed as fraction w.r.t 
	fs and window size(= nfft) respectively
	'''
	L	= len(x)
	if (nfft*fs)%2==0:
		N	= int(nfft*fs)
	else:
		print "Warning, odd/non-integer window length"
		print nfft*fs
	stpsize	= int(N - N*ovlap)
	winidx	= range(0,L-N+1,stpsize)
	freqs	= np.fft.fftfreq(N,1./fs)
	df	= freqs[1] - freqs[0]
	nfreqs	= len(freqs)
	Pxy	= np.zeros([1, nfreqs],complex)
	for ii in winidx:
		xx	= x[ii: ii+ N]
		xx	= applyhanning(detrendlin(xx))
		yy	= y[ii: ii+ N]
		yy	= applyhanning(detrendlin(yy))
		fx	= getfft(xx)
		fy	= getfft(yy)
		Pxy	+= np.conj(fx)*fy * 2./(N*N*df)
		'2 for incorporating ampl(-ve freqs)'
	Pxy	= Pxy[0,0:(N)*0.5 -1]/len(winidx)
	freqs	= freqs[0:(N)*0.5 -1]
	Pxy.shape	= (len(Pxy),1)
	freqs.shape	= (len(freqs),1)
	print Pxy.shape
	print freqs.shape
	writecpsd('cpsdpy.txt',freqs,Pxy)
	return freqs, Pxy
