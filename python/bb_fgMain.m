function [f,g,p,u,res,PP,FreqVector,Px] = bb_fgMain(use_absfactor,Mics,MicPositions,MicsUsed,MicCh,CalibBase,CalibFile,Fs,ExitCh,MassFlow,Temp,Rg,Kappa,P0,Diameter,Data)

%--------------------------version consideration---------------------------
if verLessThan('matlab','7.9')
    disp('ATTENTION: You are running and old Version of Matlab. cpsd-calculation might be incorrect!');
end

%----------------------------cpsd parameters-------------------------------
nfft = Fs;
nwin = Fs;
ovlap = nwin/2;

%---------------------------flow Calculations------------------------------
c = sqrt(Kappa*Rg*(273.15+Temp));                                          %calculating speed of sound
rho = P0/(Rg*(273.15+Temp));                                               %calculating density 
area = pi*Diameter^2/4;                                                    %calculating area of tube
v=MassFlow/(rho*area);                                                     %calculating velocity
mach=v/c;                                                                  %calculating mach-number
aus=[1/(1+mach);-1/(1-mach)];                                              %calculating mach-coefficents for MMM

%-------------------Transformation to frequency domain---------------------
[Pxx,Freq] = cpsd(Data(ExitCh+1,:),Data(ExitCh+1,:),hanning(nwin),ovlap,nfft,Fs); %autospectrum exc. signal

Pxy(1:length(MicCh),1:Fs/2+1) = 0;                                         %allocate variable
for m = 1:length(MicCh)
    Pxy(m,:) = cpsd(-1*Data(MicCh(m)+1,:),Data(ExitCh+1,:),hanning(nwin),ovlap,nfft,Fs); %cross-spectral-density of the microphone signals
%     Pxy(m,:) = cpsd(1*Data(MicCh(m)+1,:),Data(ExitCh+1,:),hanning(nwin),ovlap,nfft,Fs); %cross-spectral-density of the microphone signals
end
sf = sqrt(2./Pxx);                                                         %calculate exitation part of cpsd

%-------------------------Calibrate Microphones----------------------------
back = pwd;                                                                %readout current folder
cd(CalibBase);                                                             %going to folder of calibration file
load(CalibFile);                                                           %loading of calibration file
cd(back);                                                                  %going back to last folder
if use_absfactor==0
    ABSfactor = 1;
elseif use_absfactor==-1
    ABSfactor = 0.0023;
end
calibCoeff(1:length(Mics),1:length(Freq)) = 0;                             %allocate variable
for l = 1:length(Mics);
    calibCoeff(l,:) = ABSfactor * interp1(Mics_Calib(Mics(l)).frequency',Mics_Calib(Mics(l)).Calibfactor,Freq,'linear','extrap'); %interpoation of calibration coefficient for all mics
    Pxy(l,:) = Pxy(l,:) ./ calibCoeff(l,:) .* sf' * 2/sqrt(8/3);           % calibrate mirophones, remove exitation from cpsd and correct amplitudes (von Hann window) for used mics
end

FreqVector=Mics_Calib(Mics(l)).frequency; %keyboard
Pxx = Pxx(1:Fs/2+1) ./ 1 .* sf * 2/sqrt(8/3);
%---------------find analyze frequency and build MMM-Matrix----------------
f(1:length(Mics_Calib(Mics(1)).frequency))                      = 0;       %allocating variable
g(1:length(Mics_Calib(Mics(1)).frequency))                      = 0;       %allocating variable
p(1:length(Mics_Calib(Mics(1)).frequency))                      = 0;       %allocating variable
u(1:length(Mics_Calib(Mics(1)).frequency))                      = 0;       %allocating variable
res(1:length(Mics_Calib(Mics(1)).frequency))                    = 0;       %allocating variable
PPLA(1:length(MicsUsed))                                        = 0;       %allocating variable
PP(1:length(MicsUsed),1:length(Mics_Calib(Mics(1)).frequency))  = 0;       %allocating variable

for ff = 1:length(Mics_Calib(Mics(1)).frequency);                          %frequency loop
    fext_index = Mics_Calib(Mics(1)).frequency(ff) +1;                     %exitation frequency index

    ZL=exp(-1i*2*pi*Mics_Calib(Mics(1)).frequency(ff)/c.*(MicPositions(MicsUsed)*aus')); %build MMM-Matrix

    pZL=pinv(ZL);                                                          %pseudo-inverse MMM-Matrix

    for m = 1:length(MicsUsed)
        PPLA(m) = Pxy(MicsUsed(m),fext_index);                             % create pressure vector for MMM
    end
    Px(ff) = Pxx(fext_index);
    %-------------------------Calculate f,g,p and u----------------------------

    f(ff) = pZL(1,:)*PPLA.';                                         %f and g are calculated in terms of velocity
    g(ff) = pZL(2,:)*PPLA.';                                         %f and g are calculated in terms of velocity
    p(ff) = (f(ff)+g(ff));                                           %calulation of acoustic pressure (peak value)
    u(ff) = (f(ff)-g(ff))/rho/c;                                     %calculation of acoustic velocity (peak value)

    res(ff) = norm((eye(length(MicsUsed))-ZL*pinv(ZL))*PPLA.')/norm(PPLA.');%calculation of error in MMM
    PP(1:length(MicsUsed),ff)    = PPLA;
end

end
