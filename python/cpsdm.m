clear all
data    = dlmread('testsignal.txt');
fs      = 8192;

nfft    = 3;
nwin    = 3;
ovlap   = 0.5;
[Pa,fa]   = cpsd(data(:,1),data(:,2),hanning(nwin*fs),ovlap*nwin*fs,nfft*fs,fs);

% dlmwrite('cpsdmr.txt',[f real(P) imag(P)],'delimiter','\t');
% [fb,Pb] = cpsdmat(data(:,1), data(:,2), fs, nwin, nfft, 0.5);

% figure(99);plot(fa,abs(Pa),'o-r',fb,abs(Pb),'.')
figure(99);plot(fa,abs(Pa),'o-r');
set(gca,'XLim',[308 322]);

% AH. in cpsd, the window length and length of the data for fft are
% independently controlled via nwin and nfft

ffname = 'cpsdmat_mat-py1.txt';
fid = fopen(ffname, 'w');
fprintf(fid, '%s %s %s %s %s\n', '# fs-nfft-nwin(hanning)-ovlap:', num2str(fs), num2str(nfft), num2str(nwin), num2str(ovlap));
fprintf(fid, '%s\t%-8s\t%-8s\n','# Freq,', 'Pr,', 'Pi');
mymat = [fa real(Pa) imag(Pa)];
fprintf(fid,'%-7g\t%-8f\t%-8f\n',mymat.');
fclose(fid);
