import numpy as np

PI	= 22./7

T	= 2
fs	= 8192
tt	= np.linspace(0,T,T*fs)

# sine wave
sine	= np.sin(2*PI*315*tt)

# signal
shft	= 6
shftdsin= np.lib.pad(sine,(shft,0),'constant')
noise	= np.random.random(T*fs)
yy	= (noise+shftdsin[shft:])

# signal variance
yy2	= [ii**2 for ii in yy]
varyy	= np.sum(yy2)

# fft
Y	= np.fft.fft(yy)
Y2	= [(abs(ii))**2 for ii in Y]
varY	= np.sum(Y2)

print varyy
print varY/(T*fs)
print len(Y)
print "The two values should be equal (Parseval's theorem)"
