import numpy as np
import fn_cpsd
import fn_spec

fs	= 8192
#PI	= 22./7

#T	= 8
#tt	= np.linspace(0,T,T*fs)

## sine wave
#sine	= np.sin(2*PI*315*tt)

## signal
#shft	= 6
#shftdsin= np.lib.pad(sine,(shft,0),'constant')
#noise	= np.random.random(T*fs)
#yy	= (noise+shftdsin[:-shft])

#data	= np.vstack((sine,yy)).T

## save signal for analysis in Matlab
#np.savetxt('testsignal.txt',data,delimiter='\t')

with open('testsignal.txt','r') as fid:
    data = np.loadtxt(fid)
# cpsd
nfft = 1
ovlap= 0.5
f, P = fn_cpsd.main(data[:,1],data[:,0],fs=fs,nfft=nfft,ovlap=ovlap)

headr1  = "(py) fs-nwin(nfft)-ovlap: "+str(fs)+" "+str(nfft)+" "+str(ovlap)
headr2  = "fs, Pr, Pi"
with open('cpsd_py-py2.txt','w') as fid:
    np.savetxt(fid,[],header=headr1)
    np.savetxt(fid,[],header=headr2)
    np.savetxt(fid,np.hstack((f,P.real,P.imag)),fmt="%10.8g, %10.8f, %10.8f")

# mlab.csd (for magnitude. Note, nfft := 1, no cross psd, ovlap := 0.5)
SP  = fn_spec.fn_spec(data[:,1],fs)
headr1  = "(mlab.csd) nfft=1, fs: "+str(fs)
headr2  = "fs, Pr"
with open('csd_py-py2.txt','w') as fid:
    np.savetxt(fid,[],header=headr1)
    np.savetxt(fid,[],header=headr2)
    np.savetxt(fid,SP,fmt="%10.8g, %10.8f")

# testing bb_fg process to get amplitude
f, Pxx = fn_cpsd.main(data[:,0],data[:,0], fs=fs, nfft=nfft, ovlap=ovlap)
sf = (2/Pxx)**0.5
py = P*sf*2/((8./3)**0.5)
with open('test.txt', 'w') as fid:
    np.savetxt(fid,np.column_stack((abs(Pxx),abs(py))))
